Program which shows overview of a running system.

The program is small, simple, readable and having these constraint giving rich
and practical information about the system.

It is a toy project inspired by old programs like xosview.

Annother goal of the project is to implement the program in 3 versions:
  - using C language only, 
  - using C++ only, 
  - and using Ada langueage only.
I am going to describe those experience.
